import React from "react"

import Layout from "../components/layout"
import Heading from "../components/heading"
import ExternalLink from "../components/external-link"

export default () => (
  <Layout title="tech">
    <Heading>TUTT</Heading>
    <div>
      <p>Hi, I'm Jim.</p>

      <p>
        Lovingly abbreviated as TUTT, tightenupthe.tech tracks my engineering
        efforts.
      </p>

      <p>
        I am a multidisciplinary engineer working in the web space at this point
        in time. I have worked across the entire stack, been responsible for
        leading small - medium sized teams and have both run and scheduled
        projects using the agile methodology.
      </p>

      <p>
        Outside of work I enjoy comic books, video games, science fiction &amp;
        fantasy and just about any other stereotyped hobby you can think of.
      </p>

      <p>
        Please feel free to reach out via email{" "}
        <ExternalLink href="mailto:jim@tightenupthe.tech">
          [jim@tightenupthe.tech]
        </ExternalLink>{" "}
        or twitter{" "}
        <ExternalLink href="https://twitter.com/dammitjim1">
          [@dammitjim1]
        </ExternalLink>
        .
      </p>
    </div>
  </Layout>
)
