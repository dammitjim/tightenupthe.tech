import React from "react"

import Layout from "../components/layout"
import Heading from "../components/heading"
import ExternalLink from "../components/external-link"

export default () => (
  <Layout title="workflows">
    <Heading>What I do</Heading>
    <div>
      <p>
        For the last two years I have worked as the Principal Engineer of the
        software consultancy{" "}
        <ExternalLink href="https://omni-digital.co.uk">
          Omni Digital
        </ExternalLink>
        . I work primarily with Python and the Django framework.
      </p>

      <p>
        At my time there I have rebuilt the way infrastructure is managed
        through Ansible and Terraform, built a team of 2 into a productive group
        of 10, delivered a variety of client facing projects &amp; managed the
        workloads. context and scheduling of the engineering team.
      </p>

      <p>
        Prior to this, I worked for the startup{" "}
        <ExternalLink href="http://fresh8gaming.com/">
          Fresh8 Gaming
        </ExternalLink>{" "}
        as a part of the backend team. I took part in the construction of
        business critical APIs and the analysis, then rebuild of the primary
        Golang advertising server as a part of a microservice architecture.
      </p>
    </div>
  </Layout>
)
