import React from "react"

import Layout from "../components/layout"
import Heading from "../components/heading"
import ExternalLink from "../components/external-link"

export default () => (
  <Layout title="documentation">
    <Heading>Don’t write a style guide, just format your damn code.</Heading>
    <div>
      <p>It's 2019 and your feelings on code style do not matter.</p>

      <p>
        Everyone will read code differently, maybe you have your own way of
        doing things when you are working on personal projects, maybe you just
        really do not like how Jerry puts a new line before the bracket of a
        function .When you are working in a team it is imperative that we all
        move past our individual differences to find a common ground of what
        code should look like. This is something that almost all professionals
        agree on, at least in principle.
      </p>

      <p>
        <ExternalLink href="https://medium.com/@dammitjim1/dont-write-a-style-guide-just-format-your-damn-code-7cf29cc9b902">
          Read more on medium.
        </ExternalLink>
      </p>
    </div>
  </Layout>
)
