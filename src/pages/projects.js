import React from "react"

import Layout from "../components/layout"
import Heading from "../components/heading"

export default () => (
  <Layout title="projects">
    <Heading>Badfeed</Heading>
  </Layout>
)
