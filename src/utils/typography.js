import Typography from "typography"
import OceanBeach from "typography-theme-ocean-beach"

const options = {
  ...OceanBeach,
  headerFontFamily: ["Roboto", "sans-serif"],
  googleFonts: [
    {
      name: "Roboto Mono",
      styles: ["400"],
    },
    {
      name: "Roboto",
      styles: ["400", "400i", "700", "700i"],
    },
    {
      name: "Roboto Condensed",
      styles: ["400i", "700i"],
    },
  ],
}

const typography = new Typography(options)

export default typography
