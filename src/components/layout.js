import React from "react";
import styled from "styled-components";

import PageTitle from "../components/page-title"
import Navigation from "../components/nav"

const Container = styled.div`
    margin: 0 auto;
    max-width: 960px;
    display: flex;
    flex-direction: column;
    padding: 0 20px;
    box-sizing: border-box;
`;

export default props => (
    <Container>
        <PageTitle title={props.title} />
        <Navigation />
        <div>
            {props.children}
        </div>
    </Container>
)
