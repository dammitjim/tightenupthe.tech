import React from "react";
import styled from "styled-components";

const ExternalLink = styled.a`
  color: #ce599f;
  font-style: italic;
  background-image: none;
  text-shadow: none;
`;


export default props => (
    <ExternalLink {...props} target="_blank">
        {props.children}
    </ExternalLink>
)
