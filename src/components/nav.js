import React from "react";
import styled from "styled-components";

import InternalLink from "./internal-link";

const Navigation = styled.nav`
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-start;
    align-items: flex-start;
    padding-bottom: 20px;
    border-bottom: 1px solid #E2E2E2;
`

export default props => (
    <Navigation>
        <InternalLink to="/">Home</InternalLink>
        <InternalLink to="/work">Work</InternalLink>
        <InternalLink to="/projects">Projects</InternalLink>
        <InternalLink to="/writing">Writing</InternalLink>
    </Navigation>
)
