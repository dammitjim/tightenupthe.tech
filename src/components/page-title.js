import React from "react"
import styled from "styled-components"

import colors from "../config/colors";

const Heading = styled.h1`
  font-family: "Roboto Mono";
  line-height: 1.2;
  margin-bottom: 40px;
`

const GreenLad = styled.span`
  color: ${colors.primary};
`

export default props => (
  <Heading>
    Tighten up the <br/><GreenLad>{props.title}.</GreenLad>
  </Heading>
)
