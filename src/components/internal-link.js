import React from "react"
import { Link } from "gatsby"
import styled from "styled-components"

import colours from "../config/colors";

const InternalLink = styled(Link)`
  color: ${colours.link};
  font-family: ${props => (props.condensed ? "Roboto Condensed" : "Roboto")};
  font-style: italic;
  font-weight: 400;
  text-decoration: none;
  background-image: none;
  text-shadow: none;
  margin-right: 15px;
`

const activeStyles = {
  textDecoration: "underline",
  fontWeight: 700
}

export default props => (
  <InternalLink activeStyle={activeStyles} {...props}>
    {props.children}
  </InternalLink>
)
