import React from "react";
import styled from "styled-components";

import colors from "../config/colors";

const Heading = styled.h2`
    color: ${colors.primary};
`

export default ({children}) => (
    <Heading>{children}</Heading>
);
